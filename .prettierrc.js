module.exports = {
	endOfLine: 'lf',
	printWidth: 100,
	semi: true,
	singleQuote: true,
	tabWidth: 4,
	trailingComma: 'none',
	useTabs: true,

	overrides: [
		{
			files: '*.json',
			options: {
				tabWidth: 2,
				useTabs: false
			}
		},
		{
			files: '*.php',
			options: {
				braceStyle: '1tbs',
				bracketSpacing: true
			}
		}
	]
};
