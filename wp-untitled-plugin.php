<?php
/**
 * Plugin Name:     WP Untitled Plugin
 * Description:     Template to create a new WordPress plugin.
 * Author:          Rudy Susanto
 * Author URI:      https://profiles.wordpress.org/rsusanto/
 * Text Domain:     wp-untitled-plugin
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         WP_Untitled_Plugin
 */

final class WP_Untitled_Plugin {
	/**
	 * Singleton pattern instance.
	 *
	 * @var WP_Untitled_Plugin
	 */
	private static $instance = null;

	/**
	 * Singleton pattern method.
	 *
	 * @return WP_Untitled_Plugin
	 */
	public static function instance() {
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Default constructor.
	 */
	private function __construct() {
		$this->base_dir = plugin_dir_path(__FILE__);
		$this->base_url = plugin_dir_url(__FILE__);

		spl_autoload_register(array($this, 'autoloader'));

		if (is_admin()) {
			new WP_Untitled_Plugin_Admin();
		}
	}

	/**
	 * Class autoloader.
	 *
	 * @param string $class The name of the class to be loaded.
	 */
	public function autoloader($class) {
		if (0 !== strpos($class, 'WP_Untitled_Plugin_')) {
			return;
		}

		$class = strtolower($class);
		$path = $this->base_dir . 'includes' . DIRECTORY_SEPARATOR;
		$file = $path . 'class-' . str_replace('_', '-', $class) . '.php';

		if (file_exists($file)) {
			require_once $file;
		}
	}

	/**
	 * Get URL of a stylesheet file.
	 *
	 * @param string $name
	 * @return string
	 */
	public function get_css($name) {
		$sep = DIRECTORY_SEPARATOR;
		$url = $this->base_url . 'assets' . $sep . 'css' . $sep . $name . '.css';

		return $url;
	}

	/**
	 * Get URL of a javascript file.
	 *
	 * @param string $name
	 * @return string
	 */
	public function get_js($name) {
		$sep = DIRECTORY_SEPARATOR;
		$url = $this->base_url . 'assets' . $sep . 'js' . $sep . $name . '.js';

		return $url;
	}

	/**
	 * Get template string.
	 *
	 * @param string $name Template name.
	 * @param array  $data Optional data to be passed to the template.
	 * @return string
	 */
	public function get_template($name, $data = array()) {
		$file = $this->base_dir . 'templates' . DIRECTORY_SEPARATOR . $name . '.php';
		$file = apply_filters('wp_untitled_plugin_template', $file, $name);

		$template = '';

		if (file_exists($file)) {
			try {
				ob_start();
				include $file;
				$template = ob_get_clean();
			} catch (Exception $e) {
				// Do nothing.
			}
		}

		return $template;
	}
}

function WP_Untitled_Plugin() {
	return WP_Untitled_Plugin::instance();
}

WP_Untitled_Plugin();
